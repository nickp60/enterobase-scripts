#! /usr/bin/env python
"""
Tests your EnteroBase API token with a simple request.

### CHANGE LOG ###
2019-04-20 Nick WAters <nickp60@gmail.com>
    * Move to using requests library, and python3
2016-01-19 Nabil-Fareed Alikhan <n-f.alikhan@warwick.ac.uk>
    * Initial build
2016-08-30 Nabil-Fareed Alikhan <n-f.alikhan@warwick.ac.uk>
    * Updated to API 2.0
"""

import os
import requests
from requests.auth import HTTPBasicAuth
import logging
import traceback
import time
import argparse
import json
import sys

__epi__ = "Licence: GPLv3 by Nabil-Fareed Alikhan <n-f.alikhan@warwick.ac.uk>"
__version__ = '0.0.2'

# You must have a valid API Token
API_TOKEN = os.getenv('ENTEROBASE_API_TOKEN', None)
SERVER_ADDRESS = 'http://enterobase.warwick.ac.uk'


def test_api():
    if API_TOKEN:
        global args
        address = SERVER_ADDRESS + '/api/v2.0/senterica/straindata?assembly_status=Assembled&limit=1&offset=10000'
        response = requests.get(address, auth=HTTPBasicAuth(API_TOKEN, ""))
        if response.status_code != 200:
            logger.error('%d %s. <%s>\n Reason: %s' %(response.status_code,
                                                      response.connection,
                                                      response.url,
                                                      response.text.strip()))
            raise ValueError
        print(json.dumps(response.text.strip(), sort_keys=True, indent=4, separators=(',', ': ')))

    else:
        logger.error('No API Token, Please register at enterobase.warwick.ac.uk '
                     'and request a token from an administrator')


if __name__ == '__main__':
    try:
        start_time = time.time()
        desc = __doc__.split('\n\n')[1].strip()
        logging.basicConfig(level=logging.INFO)
        logger = logging.getLogger(__name__)

        parser = argparse.ArgumentParser(description=desc,epilog=__epi__)
        parser.add_argument ('-v', '--verbose', action='store_true', default=False, help='verbose output')
        parser.add_argument('--version', action='version', version='%(prog)s ' + __version__)
        args = parser.parse_args()
        if args.verbose: print ("Executing @ " + time.asctime())

        test_api()

        if args.verbose: print ("Ended @ " + time.asctime())
        if args.verbose: print ('total time in minutes:')
        if args.verbose: print ((time.time() - start_time) / 60.0)
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print ('ERROR, UNEXPECTED EXCEPTION')
        print (str(e))
        traceback.print_exc()
        os._exit(1)
