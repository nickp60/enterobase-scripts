enterobase-scripts
==================
**Scripts to help you make the most of EnteroBase (http://enterobase.warwick.ac.uk)**

You will require a valid **API Token** to download data from EnteroBase. To get a token:

* Create an account on EnteroBase (http://enterobase.warwick.ac.uk)
* Email me at (n-f.alikhan@warwick.ac.uk) with your username or email you registered with and which database you
would like access to.
* The token will be available on the home page of the database e.g. (http://enterobase.warwick.ac.uk/species/index/senterica)

Need more explanation? [Click Here](https://bitbucket.org/enterobase/enterobase-web/wiki/Getting%20started%20with%20Enterobase%20API)

Found a bug or want to suggest an improvement? [Post here](https://bitbucket.org/enterobase/enterobase-scripts/issues?status=new&status=open)


##Examples in this package:

* Worked Example 0: Testing the API with your TOKEN
* Worked Example 1: Downloading all MLST Alleles sequences, profiles and ST Specifically for EnteroBase schemes including 
  7 Gene MLST [University of Warwick (formally UCC) MLST Schemes curated by Mark Achtman.](http://mlst.warwick.ac.uk)
* Worked Example 2: Daily daemon script to download all MLST Alleles sequences, profiles and ST into Bionumerics.
* Worked Example 3: Downloading genome assemblies


## Citation & Legal

These scripts are licenced under GPLv3.

rMLST is described in [Jolley et al. 2012 Microbiology 158:1005-15.](http://www.ncbi.nlm.nih.gov/pubmed/22282518)
rMLST is Copyright (C) 2010-2011, University of Oxford.

You can not retrieve rMLST alleles sequences or do bulk downloads of STs.
Only rMLST Allele numbers and STs for strains within EnteroBase are available.